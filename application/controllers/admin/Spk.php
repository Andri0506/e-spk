<?php
class Spk extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url();
            redirect($url);
        };
		$this->load->model('m_kategori');
		$this->load->model('m_barang');
		$this->load->model('m_spk');
	}
	function index(){
	if($this->session->userdata('akses')=='1'){
		
		$data['data']=$this->m_barang->tampil_barang();
		$data['kat']=$this->m_kategori->tampil_kategori();
		$data['kat2']=$this->m_kategori->tampil_kategori();
		$this->load->view('admin/v_spk',$data);
	}else{
        echo "Halaman tidak ditemukan";
    }
	}
	function desc_pekerjaan_spk(){
	if($this->session->userdata('akses')=='1'){
		// $where = array('spk_nofak' => $id);
 		// $data['nf'] = $this->m_spk->get_data($where,'tbl_spk')->result();
		// $nofak=$this->m_spk->get_nofak();
		// $this->session->set_userdata('nofak',$nofak);
		$data['data']=$this->m_barang->tampil_barang();
		$data['kat']=$this->m_kategori->tampil_kategori();
		$data['kat2']=$this->m_kategori->tampil_kategori();
		$this->load->view('admin/desc_pekerjaan_spk',$data);
	}else{
        echo "Halaman tidak ditemukan";
    }
	}
	function get_barang(){
	if($this->session->userdata('akses')=='1'){
		$kobar=$this->input->post('kode_brg');
		$nabar=$this->input->post('nama_brg');
		$x['brg']=$this->m_barang->get_barang($kobar,$nabar);
		$x['kat']=$this->m_kategori->tampil_kategori();
		$x['kat2']=$this->m_kategori->tampil_kategori();
		$this->load->view('admin/v_detail_barang_spk',$x);
	}else{
        echo "Halaman tidak ditemukan";
    }
	}
	function add_to_cart(){
	if($this->session->userdata('akses')=='1'){
		$nabar=$this->input->post('nama_brg');
		$kobar=$this->input->post('kode_brg');
		$produk=$this->m_barang->get_barang($kobar,$nabar);
		$i=$produk->row_array();
		$data = array(
               'id'       => $i['barang_id'],
               'name'     => $i['barang_nama'],
			   'ukuran'      => $this->input->post('ukuran'),
               'qty'      => $this->input->post('qty'),
			   'nama_pekerja'      => $this->input->post('nama_pekerja')
            );
	if(!empty($this->cart->total_items())){
		foreach ($this->cart->contents() as $items){
			$id=$items['id'];
			$qtylama=$items['qty'];
			$rowid=$items['rowid'];
			$qty=$this->input->post('qty');
			if($id==$nabar){
				$up=array(
					'rowid'=> $rowid,
					'qty'=>$qtylama+$qty
					);
				$this->cart->update($up);
			}else{
				$this->cart->insert($data);
			}
		}
	}else{
		$this->cart->insert($data);
	}
		// redirect('admin/spk/');
		redirect('admin/spk/desc_pekerjaan_spk');
	}else{
        echo "Halaman tidak ditemukan";
    }
	}
	function remove(){
	if($this->session->userdata('akses')=='1'){
		$row_id=$this->uri->segment(4);
		$this->cart->update(array(
               'rowid'      => $row_id,
               'qty'     => 0
            ));
		redirect('admin/spk/desc_pekerjaan_spk');
	}else{
        echo "Halaman tidak ditemukan";
    }
	}
	function simpan_spk(){
	if($this->session->userdata('akses')=='1'){
		$no_po=$this->input->post('no_po');
		$nama_pekerjaan=$this->input->post('nama_pekerjaan');
		$lokasi=$this->input->post('lokasi');
		$nofak=$this->m_spk->get_nofak();
		$this->session->set_userdata('nofak',$nofak);
		$order_proses=$this->m_spk->simpan_spk($nofak,$no_po,$nama_pekerjaan,$lokasi);
		    if($order_proses){
				$this->cart->destroy();
					
				$this->session->unset_userdata('tglfak');
				$this->load->view('admin/alert/alert_sukses');	
			}else{
				redirect('admin/spk');
			}
		echo $this->session->set_flashdata('msg','<label class="label label-danger">SPK Gagal di Simpan, Mohon Periksa Kembali Semua Inputan Anda!</label>');
		// $this->load->view('admin/alert/alert_sukses');

	}else{
        echo "Halaman tidak ditemukan";
    }
	}

	function simpan_desc_perkerjaan_spk(){
	if($this->session->userdata('akses')=='1'){
		$nofak=$this->m_spk->get_nofak_penerbitan();
		$this->session->set_userdata('nofak',$nofak);
		$order_proses=$this->m_spk->simpan_desc_perkerjaan_spk($nofak);
		    if($order_proses){
				$this->cart->destroy();
					
				$this->session->unset_userdata('tglfak');
				$this->load->view('admin/alert/alert_sukses_desc_pekerjaan');	
			}else{
				redirect('admin/spk');
			}
		echo $this->session->set_flashdata('msg','<label class="label label-danger">SPK Gagal di Simpan, Mohon Periksa Kembali Semua Inputan Anda!</label>');
		// $this->load->view('admin/alert/alert_sukses');

	}else{
        echo "Halaman tidak ditemukan";
    }
	}

	function penerbitan_spk(){
	if($this->session->userdata('akses')=='1'){
		$no_surat=$this->input->post('no_surat');
		$nama_pekerjaan=$this->input->post('nama_pekerjaan');
		$lokasi=$this->input->post('lokasi');
		$nofak=$this->m_spk->get_nofak();
		$this->session->set_userdata('nofak',$nofak);
		$order_proses=$this->m_spk->penerbitan_spk($nofak,$no_surat,$nama_pekerjaan,$lokasi);
		    if($order_proses){
				$this->cart->destroy();
					
				$this->session->unset_userdata('tglfak');
				$this->load->view('admin/alert/alert_sukses');	
			}else{
				redirect('admin/spk');
			}
		echo $this->session->set_flashdata('msg','<label class="label label-danger">SPK Gagal di Simpan, Mohon Periksa Kembali Semua Inputan Anda!</label>');
		// $this->load->view('admin/alert/alert_sukses');

	}else{
        echo "Halaman tidak ditemukan";
    }
	}

	function cetak_faktur(){
		$x['data']=$this->m_spk->cetak_faktur();
		$this->load->view('admin/laporan/v_faktur',$x);
		//$this->session->unset_userdata('nofak');
	}

	function cetak_penerbitan_spk(){
		$x['data']=$this->m_spk->cetak_penerbitan_spk();
		$this->load->view('admin/laporan/v_lap_penerbitan_spk',$x);
		//$this->session->unset_userdata('nofak');
	}

	function cetak_desc_pekerjaan(){
		$x['data']=$this->m_spk->cetak_desc_pekerjaan();
		$this->load->view('admin/laporan/v_lap_desc_pekerjaan',$x);
		//$this->session->unset_userdata('nofak');
	}

	function view_spk(){
		if($this->session->userdata('akses')=='1'){
			$data['data']=$this->m_spk->tampil_spk();
			$this->load->view('admin/v_penerbitan_spk',$data);
		}else{
			echo "Halaman tidak ditemukan";
		}
	}

	function tampil_spk(){
		if($this->session->userdata('akses')=='2'){
			$data['data']=$this->m_spk->tampil_spk();
			$this->load->view('admin/v_tampil_spk',$data);
		}else{
			echo "Halaman tidak ditemukan";
		}
	}

	function view_detail_spk($id){
		if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='2'){
			
			$data['data']=$this->m_spk->view_spk($id);
			$this->load->view('admin/v_view_spk',$data);
		}else{
			echo "Halaman tidak ditemukan";
		}
	}
	function hapus_barang(){
		if($this->session->userdata('akses')=='2'){
			$kode=$this->input->post('kode');
			$this->m_barang->hapus_barang($kode);
			redirect('admin/detail_spk');
		}else{
			echo "Halaman tidak ditemukan";
		}
		}


}