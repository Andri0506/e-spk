<?php
class Laporan extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url();
            redirect($url);
        };
		$this->load->model('m_kategori');
		$this->load->model('m_barang');
		$this->load->model('m_laporan');
	}
	function index(){
		if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='2'){
			$data['data']=$this->m_barang->tampil_barang();
			$data['kat']=$this->m_kategori->tampil_kategori();
			// $data['permintaan_bln']=$this->m_laporan->get_bulan_permintaan();
			// $data['permintaan_thn']=$this->m_laporan->get_tahun_permintaan();
			$this->load->view('admin/laporan/v_lap_pimpinan',$data);
		}else{
			echo "Halaman tidak ditemukan";
		}
	}

	function lap_spk(){
		$data['data']=$this->m_laporan->tampil_spk();
		$data['kat']=$this->m_kategori->tampil_kategori();
		$this->load->view('admin/laporan/v_lap_pimpinan_spk',$data);
	}

	function view_detail_spk($id){		
		$data['data']=$this->m_laporan->lap_detail_spk($id);
		$this->load->view('admin/laporan/v_lap_detail_spk',$data);
	}

	function lap_permintaan_barang(){
		$data['data']=$this->m_laporan->tampil_permintaan_barang();
		$data['kat']=$this->m_kategori->tampil_kategori();
		$this->load->view('admin/laporan/v_lap_pimpinan_p_b',$data);
	}

	function view_detail_permintaan_barang($id){		
		$data['data']=$this->m_laporan->lap_detail_permintaan_barang($id);
		$this->load->view('admin/laporan/v_lap_detail_p_b',$data);
	}
	
	function lap_permintaan_barang_a_k(){
		$data['data']=$this->m_laporan->tampil_permintaan_barang();
		$data['kat']=$this->m_kategori->tampil_kategori();
		$this->load->view('admin/laporan/v_lap_ak',$data);
	}

	function view_detail_permintaan_barang_a_k($id){		
		$data['data']=$this->m_laporan->lap_detail_permintaan_barang_a_k($id);
		$this->load->view('admin/laporan/v_lap_detail_permintaan_b_a_k',$data);
	}

	function lap_permintaan_barang_a_g(){
		$data['data']=$this->m_laporan->tampil_permintaan_barang_a_g();
		$data['kat']=$this->m_kategori->tampil_kategori();
		$this->load->view('admin/laporan/v_lap_ag',$data);
	}

	function view_detail_permintaan_barang_a_g($id){		
		$data['data']=$this->m_laporan->lap_detail_permintaan_barang_a_g($id);
		$this->load->view('admin/laporan/v_lap_detail_permintaan_b_a_g',$data);
	}

	function lap_data_barang(){
		$x['data']=$this->m_laporan->get_data_barang();
		$this->load->view('admin/laporan/v_lap_barang',$x);
	}

	function lap_data_permintaan_barang(){
		$x['data']=$this->m_laporan->get_data_permintaan_barang();
		$this->load->view('admin/laporan/v_lap_permintaan_barang',$x);
	}

	

	// function lap_penjualan_pertanggal(){
	// 	$tanggal=$this->input->post('tgl');
	// 	$x['jml']=$this->m_laporan->get_data__total_jual_pertanggal($tanggal);
	// 	$x['data']=$this->m_laporan->get_data_jual_pertanggal($tanggal);
	// 	$this->load->view('admin/laporan/v_lap_jual_pertanggal',$x);
	// }
	// function lap_penjualan_perbulan(){
	// 	$bulan=$this->input->post('bln');
	// 	$x['jml']=$this->m_laporan->get_total_jual_perbulan($bulan);
	// 	$x['data']=$this->m_laporan->get_jual_perbulan($bulan);
	// 	$this->load->view('admin/laporan/v_lap_jual_perbulan',$x);
	// }
	// function lap_penjualan_pertahun(){
	// 	$tahun=$this->input->post('thn');
	// 	$x['jml']=$this->m_laporan->get_total_jual_pertahun($tahun);
	// 	$x['data']=$this->m_laporan->get_jual_pertahun($tahun);
	// 	$this->load->view('admin/laporan/v_lap_jual_pertahun',$x);
	// }

	function lap_data_spk(){
		$x['data']=$this->m_laporan->get_data_spk();
		$x['jml']=$this->m_laporan->get_total_spk();
		$this->load->view('admin/laporan/v_lap_penjualan',$x);
	}

	
}