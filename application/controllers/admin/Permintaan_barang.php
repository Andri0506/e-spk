<?php
class Permintaan_barang extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url();
            redirect($url);
        };
		$this->load->model('m_kategori');
		$this->load->model('m_barang');
		$this->load->model('m_permintaan_barang');
	}
	function index(){
	if($this->session->userdata('akses')=='2'){
		$data['data']=$this->m_barang->tampil_barang();
		$data['kat']=$this->m_kategori->tampil_kategori();
		$data['kat2']=$this->m_kategori->tampil_kategori();
		$this->load->view('admin/v_permintaan_barang',$data);
	}else{
        echo "Halaman tidak ditemukan";
    }
	}
	function get_barang(){
	if($this->session->userdata('akses')=='2'){
		$kobar=$this->input->post('kode_brg');
		$nabar=$this->input->post('nama_brg');
		$x['brg']=$this->m_barang->get_barang($kobar,$nabar);
		$x['kat']=$this->m_kategori->tampil_kategori();
		$x['kat2']=$this->m_kategori->tampil_kategori();
		$this->load->view('admin/v_detail_permintaan_barang',$x);
	}else{
        echo "Halaman tidak ditemukan";
    }
	}
	function add_to_cart(){
	if($this->session->userdata('akses')=='2'){
		$kobar=$this->input->post('kode_brg');
		$nabar=$this->input->post('nama_brg');
		$produk=$this->m_barang->get_barang($kobar,$nabar);
		$i=$produk->row_array();
		$data = array(
               'id'       => $i['barang_id'],
               'name'     => $i['barang_nama'],
			   'kat_nm'   => $i['kategori_nama'],
               'qty'      => $this->input->post('qty')
            );
	if(!empty($this->cart->total_items())){
		foreach ($this->cart->contents() as $items){
			$id=$items['id'];
			$qtylama=$items['qty'];
			$rowid=$items['rowid'];
			// $kobar=$this->input->post('kode_brg');
			$qty=$this->input->post('qty');
			if($id==$kobar){
				$up=array(
					'rowid'=> $rowid,
					'qty'=>$qtylama+$qty
					);
				$this->cart->update($up);
			}else{
				$this->cart->insert($data);
			}
		}
	}else{
		$this->cart->insert($data);
	}
		redirect('admin/permintaan_barang');
	}else{
        echo "Halaman tidak ditemukan";
    }
	}
	function remove(){
	if($this->session->userdata('akses')=='2'){
		$row_id=$this->uri->segment(4);
		$this->cart->update(array(
               'rowid'      => $row_id,
               'qty'     => 0
            ));
		redirect('admin/permintaan_barang');
	}else{
        echo "Halaman tidak ditemukan";
    }
	}
	function simpan_permintaan_barang(){
	if($this->session->userdata('akses')=='2'){	
		$nofak=$this->m_permintaan_barang->get_nofak();
		$this->session->set_userdata('nofak',$nofak);
		$order_proses=$this->m_permintaan_barang->simpan_permintaan_barang($nofak);
		    if($order_proses){
				$this->cart->destroy();
					
				$this->session->unset_userdata('tglfak');
				$this->load->view('admin/alert/alert_permintaan_barang');	
			}else{
				redirect('admin/permintaan_barang');
			}
		echo $this->session->set_flashdata('msg','<label class="label label-danger">SPK Gagal di Simpan, Mohon Periksa Kembali Semua Inputan Anda!</label>');
		// $this->load->view('admin/alert/alert_sukses');

	}else{
        echo "Halaman tidak ditemukan";
    }
	}

	function cetak_faktur(){
		$x['data']=$this->m_permintaan_barang->cetak_faktur();
		$this->load->view('admin/laporan/v_faktur',$x);
		//$this->session->unset_userdata('nofak');
	}

	function detail_permintaan_barang(){
		if($this->session->userdata('akses')=='3'){
			$data['data']=$this->m_permintaan_barang->tampil_detail_permintaan_barang();
			// $data['kat']=$this->m_kategori->tampil_kategori();
			// $data['kat2']=$this->m_kategori->tampil_kategori();
			$this->load->view('admin/v_data_permintaan_barang',$data);
		}else{
			echo "Halaman tidak ditemukan";
		}
	}

	function tampil_permintaan_barang(){
		if($this->session->userdata('akses')=='2' || $this->session->userdata('akses')=='3'){
			$data['data']=$this->m_permintaan_barang->tampil_permintaan_barang();
			$this->load->view('admin/v_d_permintaan_b',$data);
			// print_r($data);
		}else{
			echo "Halaman tidak ditemukan";
		}
	}

	function view_permintaan_barang($id){
		if($this->session->userdata('akses')=='2' || $this->session->userdata('akses')=='3'){
			// $id = $this->uri->segment(3);
			$data['data']=$this->m_permintaan_barang->view_permintaan_barang($id);
			$this->load->view('admin/v_d_permintaan_b2',$data);
			// print_r($data);
		}else{
			echo "Halaman tidak ditemukan";
		}
	}

	function update_permintaan_barang($fk){
		if($this->session->userdata('akses')=='3'){
			$this->m_permintaan_barang->update_permintaan_barang2($fk);
			//print_r($this->input->post());

			$jumlahData = intval ($this->input->post("jumlah_data"));
			// echo $jumlahData;
			if($jumlahData > 0) {
				for($i=1;$i<=$jumlahData;$i++) {
					$a = $this->input->post('d_permintaan_barang_nofak_' . strval($i));
					$b = $this->input->post('d_permintaan_qty_' . strval($i));
					$c = $this->input->post('d_permintaan_id_' . strval($i));

					// echo $a . "/" . $b . "/" . $c . "</br>";
					$result = $this->db->query("UPDATE tbl_detail_permintaan_barang SET d_permintaan_qty=$b WHERE d_permintaan_barang_nofak=$a");
				}
				// if($result){
				// $this->load->view('admin/alert/alert_sukses_permintaan');
				// 	// print_r($result);
				// }else{
				// 	echo "update gagal";
				// }
			}
			// $this->load->view('admin/alert/alert_sukses_permintaan');
			redirect('admin/permintaan_barang/tampil_permintaan_barang');
		}else{
			echo "Halaman tidak ditemukan";
		}
	}

	function edit_detail_permintaan_barang(){
		if($this->session->userdata('akses')=='3'){
			$id=$this->input->post('d_permintaan_id');
			$fk=$this->input->post('d_permintaan_barang_nofak');
			$permintaan_brng_id=$this->input->post('d_permintaan_barang_id');
			$permintaan_brng_nama=$this->input->post('d_permintaan_barang_nama');
			$permintaan_qty=$this->input->post('d_permintaan_qty');
			$status_permintaan_barang=$this->input->post('status_permintaan_barang');
			$this->m_permintaan_barang->update_detail_permintaan_barang($id,$fk,$permintaan_brng_id,$permintaan_brng_nama,$permintaan_qty,$status_permintaan_barang);
			redirect('admin/Permintaan_barang/v_d_permintaan_b');
		}else{
			echo "Halaman tidak ditemukan";
		}
	}

	function ag_d_p_barang(){
		if($this->session->userdata('akses')=='4'){
			$data['data']=$this->m_permintaan_barang->tampil_d_p_barang_ag();
			$this->load->view('admin/v_data_permintaan_barang_ag',$data);
		}else{
			echo "Halaman tidak ditemukan";
		}
	}
	
	function view_ag_d_p_barang($id){
		if($this->session->userdata('akses')=='4'){
			$data['data']=$this->m_permintaan_barang->view_p_barang_ag($id);
			$this->load->view('admin/v_view_permintaan_b_ag',$data);
		}else{
			echo "Halaman tidak ditemukan";
		}
	}

	function update_permintaan_barang_ag($fk){
		if($this->session->userdata('akses')=='4'){
			// $fk=$this->input->post('d_permintaan_barang_nofak');
			$permintaan_barang_keterangan=$this->input->post('permintaan_barang_keterangan');
			$this->m_permintaan_barang->update_permintaan_barang_ag($fk,$permintaan_barang_keterangan);
			$this->load->view('admin/alert/alert_sukses_permintaan');
		}else{
			echo "Halaman tidak ditemukan";
		}
	}

}