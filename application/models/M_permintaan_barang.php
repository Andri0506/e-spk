<?php
class M_permintaan_barang extends CI_Model{

	function hapus_retur($kode){
		$hsl=$this->db->query("DELETE FROM tbl_retur WHERE retur_id='$kode'");
		return $hsl;
	}

	function tampil_retur(){
		$hsl=$this->db->query("SELECT retur_id,DATE_FORMAT(retur_tanggal,'%d/%m/%Y') AS retur_tanggal,retur_barang_id,retur_barang_nama,retur_barang_satuan,retur_harjul,retur_qty,(retur_harjul*retur_qty) AS retur_subtotal,retur_keterangan FROM tbl_retur ORDER BY retur_id DESC");
		return $hsl;
	}

	function simpan_retur($kobar,$nabar,$satuan,$harjul,$qty,$keterangan){
		$hsl=$this->db->query("INSERT INTO tbl_retur(retur_barang_id,retur_barang_nama,retur_barang_satuan,retur_harjul,retur_qty,retur_keterangan) VALUES ('$kobar','$nabar','$satuan','$harjul','$qty','$keterangan')");
		return $hsl;
	}

	function simpan_permintaan_barang($nofak){
		$idadmin=$this->session->userdata('idadmin');
		$this->db->query("INSERT INTO tbl_permintaan_barang (permintaan_barang_nofak,permintaan_barang_user_id,permintaan_barang_keterangan) VALUES ('$nofak','$idadmin','proses')");
		foreach ($this->cart->contents() as $item) {
			$data=array(
				'd_permintaan_barang_nofak' =>	$nofak,
				'd_permintaan_barang_id'	=>	$item['id'],
				'd_permintaan_barang_nama'	=>	$item['name'],
				'd_permintaan_qty'	=>	$item['qty']
			);
			$this->db->insert('tbl_detail_permintaan_barang',$data);
			// $this->db->query("update tbl_barang set barang_stok=barang_stok-'$item[qty]' where barang_id='$item[id]'");
		}
		return true;
	}
	function get_nofak(){
		$q = $this->db->query("SELECT MAX(RIGHT(permintaan_barang_nofak,6)) AS kd_max FROM tbl_permintaan_barang WHERE DATE(permintaan_barang_tanggal)=CURDATE()");
        $kd = "";
        if($q->num_rows()>0){
            foreach($q->result() as $k){
                $tmp = ((int)$k->kd_max)+1;
                $kd = sprintf("%06s", $tmp);
            }
        }else{
            $kd = "000001";
        }
        return date('dmy').$kd;
	}

	function cetak_faktur(){
		$nofak=$this->session->userdata('nofak');
		$hsl=$this->db->query("SELECT permintaan_barang_nofak,DATE_FORMAT(permintaan_barang_tanggal,'%d/%m/%Y %H:%i:%s') AS permintaan_barang_tanggal,permintaan_barang_keterangan,d_permintaan_barang_nama,d_permintaan_barang_qty FROM tbl_permintaan_barang JOIN tbl_detail_permintaan_barang ON permintaan_barang_nofak=d_permintaan_barang_nofak WHERE permintaan_barang_nofak='$nofak'");
		return $hsl;
	}

	function tampil_detail_permintaan_barang(){
		$hsl=$this->db->query("SELECT d_permintaan_id ,d_permintaan_barang_nofak ,d_permintaan_barang_id,d_permintaan_barang_nama,d_permintaan_qty, status_permintaan_barang FROM tbl_detail_permintaan_barang ");
		return $hsl;
	}

	function tampil_permintaan_barang(){
		$hsl=$this->db->query("SELECT permintaan_barang_nofak ,permintaan_barang_tanggal ,permintaan_barang_user_id,permintaan_barang_keterangan FROM tbl_permintaan_barang ");
		return $hsl;
	}

	function view_permintaan_barang($id){
		$query = $this->db->query("SELECT d_permintaan_id,d_permintaan_barang_nofak,d_permintaan_barang_id,d_permintaan_barang_nama, d_permintaan_qty FROM tbl_detail_permintaan_barang JOIN tbl_permintaan_barang ON permintaan_barang_nofak=d_permintaan_barang_nofak WHERE d_permintaan_barang_nofak='$id'");	
		return $query; 
	}

	function update_permintaan_barang($fk,$permintaan_qty){
		$user_id=$this->session->userdata('idadmin');
		$hsl=$this->db->query("UPDATE tbl_detail_permintaan_barang SET d_permintaan_barang_nofak='$fk',d_permintaan_qty='$permintaan_qty',status_permintaan_barang='Approve' WHERE d_permintaan_barang_nofak ='$fk'");
		return $hsl;
	}

	function update_permintaan_barang2($fk){
		$user_id=$this->session->userdata('idadmin');
		$hsl=$this->db->query("UPDATE tbl_permintaan_barang SET permintaan_barang_keterangan='Approve' WHERE permintaan_barang_nofak='$fk'");
		return $hsl;
	}

	function update_detail_permintaan_barang($id,$fk,$permintaan_brng_id,$permintaan_brng_nama,$permintaan_qty,$status_permintaan_barang){
		$user_id=$this->session->userdata('idadmin');
		$hsl=$this->db->query("UPDATE tbl_detail_permintaan_barang SET d_permintaan_barang_nofak='$fk',d_permintaan_barang_id='$permintaan_brng_id',d_permintaan_barang_nama='$permintaan_brng_nama',d_permintaan_qty='$permintaan_qty',status_permintaan_barang='$status_permintaan_barang' WHERE d_permintaan_id ='$id'");
		return $hsl;
	}

	function tampil_d_p_barang_ag(){
		// $hsl=$this->db->query("SELECT permintaan_barang_nofak ,permintaan_barang_tanggal ,permintaan_barang_user_id,permintaan_barang_keterangan FROM tbl_permintaan_barang ");
		$hsl=$this->db->query("SELECT permintaan_barang_nofak ,permintaan_barang_tanggal ,permintaan_barang_user_id,permintaan_barang_keterangan FROM tbl_permintaan_barang WHERE permintaan_barang_keterangan='Approve' OR permintaan_barang_keterangan='Disiapkan'");
		return $hsl;
	}

	function view_p_barang_ag($id){		
		$query = $this->db->query("SELECT d_permintaan_id,d_permintaan_barang_nofak,d_permintaan_barang_id,d_permintaan_barang_nama, d_permintaan_qty FROM tbl_detail_permintaan_barang JOIN tbl_permintaan_barang ON permintaan_barang_nofak=d_permintaan_barang_nofak WHERE d_permintaan_barang_nofak='$id'");
		return $query;
	}

	function update_permintaan_barang_ag($fk,$permintaan_barang_keterangan){
		$user_id=$this->session->userdata('idadmin');
		$hsl=$this->db->query("UPDATE tbl_permintaan_barang SET permintaan_barang_keterangan='$permintaan_barang_keterangan' WHERE permintaan_barang_nofak='$fk'");
		return $hsl;
	}
	
}