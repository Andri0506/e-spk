<?php
class M_spk extends CI_Model{

	function hapus_retur($kode){
		$hsl=$this->db->query("DELETE FROM tbl_retur WHERE retur_id='$kode'");
		return $hsl;
	}

	function tampil_retur(){
		$hsl=$this->db->query("SELECT retur_id,DATE_FORMAT(retur_tanggal,'%d/%m/%Y') AS retur_tanggal,retur_barang_id,retur_barang_nama,retur_barang_satuan,retur_harjul,retur_qty,(retur_harjul*retur_qty) AS retur_subtotal,retur_keterangan FROM tbl_retur ORDER BY retur_id DESC");
		return $hsl;
	}

	function simpan_retur($kobar,$nabar,$satuan,$harjul,$qty,$keterangan){
		$hsl=$this->db->query("INSERT INTO tbl_retur(retur_barang_id,retur_barang_nama,retur_barang_satuan,retur_harjul,retur_qty,retur_keterangan) VALUES ('$kobar','$nabar','$satuan','$harjul','$qty','$keterangan')");
		return $hsl;
	}

	function simpan_spk($nofak,$no_po,$nama_pekerjaan,$lokasi){
		$idadmin=$this->session->userdata('idadmin');
		$this->db->query("INSERT INTO tbl_spk (spk_nofak, no_po, nama_pekerjaan, lokasi, spk_user_id,spk_keterangan) VALUES ('$nofak','$no_po', '$nama_pekerjaan','$lokasi','$idadmin','proses')");
		foreach ($this->cart->contents() as $item) {
			$data=array(
				'd_spk_nofak' 		=>	$nofak,
				'd_spk_barang_id'	=>	$item['id'],
				'd_spk_barang_nama'	=>	$item['name'],
				'ukuran'			=>	$item['ukuran'],
				'd_spk_qty'			=>	$item['qty'],
				'nama_pekerja'		=>	$item['nama_pekerja']
			);
			$this->db->insert('tbl_detail_spk',$data);
			// $this->db->query("update tbl_barang set barang_nama=barang_nama-'$item[barang_nama]' where barang_id='$item[id]'");
		}
		return true;
	}

	function simpan_desc_perkerjaan_spk($nofak){
		$idadmin=$this->session->userdata('idadmin');
		foreach ($this->cart->contents() as $item) {
			$data=array(
				'd_spk_nofak' 		=>	$nofak,
				'd_spk_barang_id'	=>	$item['id'],
				'd_spk_barang_nama'	=>	$item['name'],
				'ukuran'			=>	$item['ukuran'],
				'd_spk_qty'			=>	$item['qty'],
			);
			$this->db->insert('tbl_detail_spk',$data);
			// $this->db->query("update tbl_barang set barang_nama=barang_nama-'$item[barang_nama]' where barang_id='$item[id]'");
		}
		return true;
	}

	function penerbitan_spk($nofak,$no_surat,$nama_pekerjaan,$lokasi){
		$idadmin=$this->session->userdata('idadmin');
		$this->db->query("INSERT INTO tbl_spk (spk_nofak, no_surat, nama_pekerjaan, lokasi, spk_user_id,spk_keterangan) VALUES ('$nofak','$no_surat', '$nama_pekerjaan','$lokasi','$idadmin','Proses')");
		return true;
	}

	function get_nofak(){
		$q = $this->db->query("SELECT MAX(RIGHT(spk_nofak,6)) AS kd_max FROM tbl_spk WHERE DATE(spk_tanggal)=CURDATE()");
        $kd = "";
        if($q->num_rows()>0){
            foreach($q->result() as $k){
                $tmp = ((int)$k->kd_max)+1;
                $kd = sprintf("%06s", $tmp);
            }
        }else{
            $kd = "000001";
        }
        return date('dmy').$kd;
	}

	function get_nofak_penerbitan(){
		$q = $this->db->query("SELECT MAX(RIGHT(d_spk_nofak,6)) AS kd_max FROM tbl_detail_spk WHERE DATE(d_spk_tanggal)=CURDATE()");
        $kd = "";
        if($q->num_rows()>0){
            foreach($q->result() as $k){
                $tmp = ((int)$k->kd_max)+1;
                $kd = sprintf("%06s", $tmp);
            }
        }else{
            $kd = "000001";
        }
        return date('dmy').$kd;
	}

	function get_data($where,$table){ 
		return $this->db->get_where($table,$where);
	}

	function cetak_faktur(){
		$nofak=$this->session->userdata('nofak');
		$hsl=$this->db->query("SELECT spk_nofak, no_po, nama_pekerjaan, lokasi, DATE_FORMAT(spk_tanggal,'%d/%m/%Y %H:%i:%s') AS spk_tanggal,spk_keterangan,d_spk_barang_nama, ukuran, d_spk_qty, nama_pekerja FROM tbl_spk JOIN tbl_detail_spk ON spk_nofak=d_spk_nofak WHERE spk_nofak='$nofak'");
		return $hsl;
	}

	function cetak_penerbitan_spk(){
		$nofak=$this->session->userdata('nofak');
		$hsl=$this->db->query("SELECT spk_nofak, no_surat, nama_pekerjaan, lokasi, DATE_FORMAT(spk_tanggal,'%d/%m/%Y %H:%i:%s') AS spk_tanggal,spk_keterangan FROM tbl_spk WHERE d_spk_nofak='$nofak'");
		return $hsl;
	}

	function cetak_desc_pekerjaan(){
		$nofak=$this->session->userdata('nofak');
		$hsl=$this->db->query("SELECT d_spk_nofak, d_spk_tanggal, d_spk_barang_id, d_spk_barang_nama, ukuran, d_spk_qty FROM tbl_detail_spk");
		return $hsl;
	}

	function tampil_spk(){
		$hsl=$this->db->query("SELECT spk_nofak, no_surat, nama_pekerjaan, lokasi, spk_tanggal,spk_user_id,spk_keterangan FROM tbl_spk ORDER BY spk_nofak DESC ");
		return $hsl;
	}
	
	function view_spk($id){
		$hsl=$this->db->query("SELECT d_spk_id,d_spk_nofak,d_spk_barang_id,d_spk_barang_nama,d_spk_qty FROM tbl_detail_spk JOIN tbl_spk ON spk_nofak=d_spk_nofak WHERE d_spk_nofak='$id'");
        return $hsl;	
	}

	function tampil_lap_spk(){
		$hsl=$this->db->query("SELECT d_spk_id,d_spk_nofak,d_spk_barang_id,d_spk_barang_nama,d_spk_qty FROM tbl_detail_spk JOIN tbl_spk ON spk_nofak=d_spk_nofak WHERE d_spk_nofak='$id'");
        return $hsl;
	}
}