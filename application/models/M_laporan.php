<?php
class M_laporan extends CI_Model{
	
	function tampil_spk(){
		$hsl=$this->db->query("SELECT spk_nofak,no_surat, nama_pekerjaan, lokasi, spk_tanggal,spk_user_id,spk_keterangan FROM tbl_spk ");
		return $hsl;
	}

	function lap_detail_spk($id){
		$hsl=$this->db->query("SELECT d_spk_id,d_spk_nofak,d_spk_barang_id,d_spk_barang_nama,d_spk_qty FROM tbl_detail_spk JOIN tbl_spk ON spk_nofak=d_spk_nofak WHERE d_spk_nofak='$id'");
        return $hsl;	
	}

	function tampil_permintaan_barang(){
		$hsl=$this->db->query("SELECT permintaan_barang_nofak,permintaan_barang_tanggal,permintaan_barang_user_id,permintaan_barang_keterangan FROM tbl_permintaan_barang");
		return $hsl;
	}

	function lap_detail_permintaan_barang($id){
		$hsl=$this->db->query("SELECT d_permintaan_id,d_permintaan_barang_nofak,d_permintaan_barang_id,d_permintaan_barang_nama,d_permintaan_qty,status_permintaan_barang,status_pengiriman_barang FROM tbl_detail_permintaan_barang JOIN tbl_permintaan_barang ON permintaan_barang_nofak=d_permintaan_barang_nofak WHERE d_permintaan_barang_nofak='$id'");
        return $hsl;	
	}
	
	function lap_detail_permintaan_barang_a_k($id){
		$hsl=$this->db->query("SELECT d_permintaan_id,d_permintaan_barang_nofak,d_permintaan_barang_id,d_permintaan_barang_nama,d_permintaan_qty,status_permintaan_barang,status_pengiriman_barang,permintaan_barang_keterangan FROM tbl_detail_permintaan_barang JOIN tbl_permintaan_barang ON permintaan_barang_nofak=d_permintaan_barang_nofak WHERE d_permintaan_barang_nofak='$id'");
        return $hsl;	
	}

	function tampil_permintaan_barang_a_g(){
		$hsl=$this->db->query("SELECT permintaan_barang_nofak,permintaan_barang_tanggal,permintaan_barang_user_id,permintaan_barang_keterangan FROM tbl_permintaan_barang WHERE permintaan_barang_keterangan='Disiapkan' OR permintaan_barang_keterangan='Pengiriman'");
		return $hsl;
	}

	function tampil_lap_spk(){
		$hsl=$this->db->query("SELECT d_spk_id,d_spk_nofak,d_spk_barang_id,d_spk_barang_nama,d_spk_qty FROM tbl_detail_spk JOIN tbl_spk ON spk_nofak=d_spk_nofak WHERE d_spk_nofak='$id'");
        return $hsl;
	}

	function get_stok_barang(){
		$hsl=$this->db->query("SELECT kategori_id,kategori_nama,barang_nama,barang_stok FROM tbl_kategori JOIN tbl_barang ON kategori_id=barang_kategori_id GROUP BY kategori_id,barang_nama");
		return $hsl;
	}
	function get_data_barang(){
		$hsl=$this->db->query("SELECT kategori_id,barang_id,kategori_nama,barang_nama,barang_tgl_input,barang_tgl_last_update,barang_kategori_id,barang_user_id FROM tbl_kategori JOIN tbl_barang ON kategori_id=barang_kategori_id GROUP BY kategori_id,barang_nama");
		return $hsl;
	}
	function get_data_permintaan_barang(){
		$hsl=$this->db->query("SELECT permintaan_barang_nofak,DATE_FORMAT(permintaan_barang_tanggal,'%d %M %Y') AS permintaan_barang_tanggal,d_permintaan_barang_id,d_permintaan_barang_nama,d_permintaan_qty FROM tbl_permintaan_barang JOIN tbl_detail_permintaan_barang ON permintaan_barang_nofak=d_permintaan_barang_nofak ORDER BY permintaan_barang_nofak DESC");
		return $hsl;
	}
	function get_data_penjualan(){
		$hsl=$this->db->query("SELECT jual_nofak,DATE_FORMAT(jual_tanggal,'%d %M %Y') AS jual_tanggal,jual_total,d_jual_barang_id,d_jual_barang_nama,d_jual_barang_satuan,d_jual_barang_harpok,d_jual_barang_harjul,d_jual_qty,d_jual_diskon,d_jual_total FROM tbl_jual JOIN tbl_detail_jual ON jual_nofak=d_jual_nofak ORDER BY jual_nofak DESC");
		return $hsl;
	}
	function get_data_spk(){
		$hsl=$this->db->query("SELECT spk_nofak,DATE_FORMAT(spk_tanggal,'%d %M %Y') AS spk_tanggal,d_spk_barang_id,d_spk_barang_nama,d_spk_qty, FROM tbl_spk JOIN tbl_detail_spk ON spk_nofak=d_spk_nofak ORDER BY spk_nofak DESC");
		return $hsl;
	}
	function get_total_penjualan(){
		$hsl=$this->db->query("SELECT jual_nofak,DATE_FORMAT(jual_tanggal,'%d %M %Y') AS jual_tanggal,jual_total,d_jual_barang_id,d_jual_barang_nama,d_jual_barang_satuan,d_jual_barang_harpok,d_jual_barang_harjul,d_jual_qty,d_jual_diskon,sum(d_jual_total) as total FROM tbl_jual JOIN tbl_detail_jual ON jual_nofak=d_jual_nofak ORDER BY jual_nofak DESC");
		return $hsl;
	}
	function get_data_jual_pertanggal($tanggal){
		$hsl=$this->db->query("SELECT jual_nofak,DATE_FORMAT(jual_tanggal,'%d %M %Y') AS jual_tanggal,d_jual_barang_id,d_jual_barang_nama,d_jual_barang_satuan,d_jual_barang_harpok,d_jual_barang_harjul,d_jual_qty,d_jual_diskon,d_jual_total FROM tbl_jual JOIN tbl_detail_jual ON jual_nofak=d_jual_nofak WHERE DATE(jual_tanggal)='$tanggal' ORDER BY jual_nofak DESC");
		return $hsl;
	}
	function get_data__total_jual_pertanggal($tanggal){
		$hsl=$this->db->query("SELECT jual_nofak,DATE_FORMAT(jual_tanggal,'%d %M %Y') AS jual_tanggal,d_jual_barang_id,d_jual_barang_nama,d_jual_barang_satuan,d_jual_barang_harpok,d_jual_barang_harjul,d_jual_qty,d_jual_diskon,SUM(d_jual_total) as total FROM tbl_jual JOIN tbl_detail_jual ON jual_nofak=d_jual_nofak WHERE DATE(jual_tanggal)='$tanggal' ORDER BY jual_nofak DESC");
		return $hsl;
	}
	function get_bulan_jual(){
		$hsl=$this->db->query("SELECT DISTINCT DATE_FORMAT(jual_tanggal,'%M %Y') AS bulan FROM tbl_jual");
		return $hsl;
	}
	function get_tahun_jual(){
		$hsl=$this->db->query("SELECT DISTINCT YEAR(jual_tanggal) AS tahun FROM tbl_jual");
		return $hsl;
	}
	function get_jual_perbulan($bulan){
		$hsl=$this->db->query("SELECT jual_nofak,DATE_FORMAT(jual_tanggal,'%M %Y') AS bulan,DATE_FORMAT(jual_tanggal,'%d %M %Y') AS jual_tanggal,d_jual_barang_id,d_jual_barang_nama,d_jual_barang_satuan,d_jual_barang_harpok,d_jual_barang_harjul,d_jual_qty,d_jual_diskon,d_jual_total FROM tbl_jual JOIN tbl_detail_jual ON jual_nofak=d_jual_nofak WHERE DATE_FORMAT(jual_tanggal,'%M %Y')='$bulan' ORDER BY jual_nofak DESC");
		return $hsl;
	}
	function get_total_jual_perbulan($bulan){
		$hsl=$this->db->query("SELECT jual_nofak,DATE_FORMAT(jual_tanggal,'%M %Y') AS bulan,DATE_FORMAT(jual_tanggal,'%d %M %Y') AS jual_tanggal,d_jual_barang_id,d_jual_barang_nama,d_jual_barang_satuan,d_jual_barang_harpok,d_jual_barang_harjul,d_jual_qty,d_jual_diskon,SUM(d_jual_total) as total FROM tbl_jual JOIN tbl_detail_jual ON jual_nofak=d_jual_nofak WHERE DATE_FORMAT(jual_tanggal,'%M %Y')='$bulan' ORDER BY jual_nofak DESC");
		return $hsl;
	}
	function get_jual_pertahun($tahun){
		$hsl=$this->db->query("SELECT jual_nofak,YEAR(jual_tanggal) AS tahun,DATE_FORMAT(jual_tanggal,'%M %Y') AS bulan,DATE_FORMAT(jual_tanggal,'%d %M %Y') AS jual_tanggal,d_jual_barang_id,d_jual_barang_nama,d_jual_barang_satuan,d_jual_barang_harpok,d_jual_barang_harjul,d_jual_qty,d_jual_diskon,d_jual_total FROM tbl_jual JOIN tbl_detail_jual ON jual_nofak=d_jual_nofak WHERE YEAR(jual_tanggal)='$tahun' ORDER BY jual_nofak DESC");
		return $hsl;
	}
	
}