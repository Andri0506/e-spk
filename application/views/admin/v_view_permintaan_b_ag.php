<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Produk By Mfikri.com">
    <meta name="author" content="M Fikri Setiadi">

    <title>E-SPK || PT TELAGA HERANG</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url().'assets/css/bootstrap.min.css'?>" rel="stylesheet">
	<!-- <link href="<?php //echo base_url().'assets/css/style.css'?>" rel="stylesheet"> -->
	<link href="<?php echo base_url().'assets/css/font-awesome.css'?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url().'assets/css/4-col-portfolio.css'?>" rel="stylesheet">
    <link href="<?php echo base_url().'assets/css/dataTables.bootstrap.min.css'?>" rel="stylesheet">
    <link href="<?php echo base_url().'assets/css/jquery.dataTables.min.css'?>" rel="stylesheet">
    <link href="<?php echo base_url().'assets/dist/css/bootstrap-select.css'?>" rel="stylesheet">
</head>

<body>
    <!-- Navigation -->
   <?php 
        $this->load->view('admin/menu');
   ?>

    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Data
                    <small>Permintaan Barang</small>
                    <div class="pull-right"><a href="<?php echo base_url().'admin/permintaan_barang/ag_d_p_barang'?>" class="btn btn-sm btn-primary"><span class="fa fa-backward"></span> Kembali</a></div>
                </h1>
            </div>
        </div>
        
        <!-- /.row -->
        <!-- Projects Row -->
        <div class="row">
            <div class="col-lg-12">
            <table class="table table-bordered table-condensed" style="font-size:11px;" id="mydata">
                <thead>
                    <tr>
                        <th style="text-align:center;width:40px;">No</th>
                        <th>Permintaan Id</th>
                        <th>Permintaan Faktur</th>
                        <th>Barang Id</th>
                        <th>Barang Nama</th>
                        <th width="100px">Permintaan Qty</th>
                        <!-- <th>Keterangan</th> -->
                        <!-- <th style="width:150px;text-align:center;">Aksi</th> -->
                    </tr>
                </thead>
                <tbody>
                <?php 
                    $no=0;
                    $no++;
                    foreach ($data->result_array() as $a) { 
                        // print_r($a);
                         
                         
                        $no++;
                        $id=$a['d_permintaan_id'];
                        $fk=$a['d_permintaan_barang_nofak'];
                        $brg_id=$a['d_permintaan_barang_id'];
                        $brg_nama=$a['d_permintaan_barang_nama'];
                        $p_qty=$a['d_permintaan_qty'];
                    ?>
                    <tr>
                        <td style="text-align:center;"><?php echo $no;?></td>
                        <td><?php echo $id;?></td>
                        <td><?php echo $fk;?></td>
                        <td style="text-align:center;"><?php echo $brg_id;?></td>
                        <td style="text-align:center;"><?php echo $brg_nama;?></td>
                        <td style="text-align:center;"><input type="number" name="qty" value="<?php echo $p_qty;?>" class="form-control" readonly></td>
                        <!-- <td style="text-align:center;"> -->
                            <!-- <a class="btn btn-xs btn-warning" href="#modalEditPermintaan<?php //echo $id?>" data-toggle="modal" title="Edit"><span class="fa fa-edit"></span> Detail</a> -->
                        <!-- </td> -->
                    </tr>
                <?php };?>
                </tbody>
            </table>
            <form action="<?php echo base_url().'admin/permintaan_barang/update_permintaan_barang_ag/'.$fk?>" method="post">
                <input type="hidden" name="d_permintaan_id" value="<?php echo $id?>">
                <input type="hidden" name="d_permintaan_barang_nofak" value="<?php echo $fk;?>">
                <!-- <input type="button" name="permintaan_barang_keterangan" value="Disiapkan" class="btn btn-xs btn-primary" style="width:100px; height:30px;"> Atau <input type="button" name="permintaan_barang_keterangan" value="Pengiriman" class="btn btn-xs btn-primary" style="width:100px; height:30px;"> -->
                <div class="form-group">
                    <div class="col-xs-9">
                        <select name="permintaan_barang_keterangan" class="form-control" style="width:280px;">
                            <option value="Disiapkan">Pilih Status Pengiriman</option>
                            <option value="Disiapkan">Disiapkan</option>
                            <option value="Pengiriman">Pengiriman</option>
                        </select>
                    </div>
                </div>
                <button type="submit" class="btn btn-sm btn-primary">Oke</button>
                <!-- </div> -->
                <!-- <input type="submit" class="btn btn-sm btn-primary" title="Oke"> -->
            </form>
            </div>
        </div>
        <!-- /.row -->
        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p style="text-align:center;">Copyright &copy; <?php echo '2021';?> by SPK || TELAGA HERANG</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="<?php echo base_url().'assets/js/jquery.js'?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url().'assets/dist/js/bootstrap-select.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/dataTables.bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/jquery.dataTables.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/jquery.price_format.min.js'?>"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#mydata').DataTable();
        } );
    </script>
</body>

</html>
