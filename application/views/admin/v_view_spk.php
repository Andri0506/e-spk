<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Produk By Mfikri.com">
    <meta name="author" content="M Fikri Setiadi">

    <title>E-SPK || PT TELAGA HERANG</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url().'assets/css/bootstrap.min.css'?>" rel="stylesheet">
	<!-- <link href="<?php //echo base_url().'assets/css/style.css'?>" rel="stylesheet"> -->
	<link href="<?php echo base_url().'assets/css/font-awesome.css'?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url().'assets/css/4-col-portfolio.css'?>" rel="stylesheet">
    <link href="<?php echo base_url().'assets/css/dataTables.bootstrap.min.css'?>" rel="stylesheet">
    <link href="<?php echo base_url().'assets/css/jquery.dataTables.min.css'?>" rel="stylesheet">
    <link href="<?php echo base_url().'assets/dist/css/bootstrap-select.css'?>" rel="stylesheet">
</head>

<body>

    <!-- Navigation -->
   <?php 
        $this->load->view('admin/menu');
   ?>

    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header">Data Detail
                    <small>SPK</small>
                    <div class="pull-right"><a href="<?=site_url('admin/spk/tampil_spk');?>" class="btn btn-sm btn-success"><span class="fa fa-backward"></span> Kembali</a></div>
                </h3>
            </div>
        </div>
        <!-- /.row -->
        <!-- Projects Row -->
        <div class="row">
            <div class="col-lg-12">
            <table class="table table-bordered table-condensed" style="font-size:11px;" id="mydata">
                <thead>
                    <tr>
                        <th style="text-align:center;width:40px;">No</th>
                        <th>SPK ID</th>
                        <th>SPK Faktur</th>
                        <th>SPK Barang Id</th>
                        <th>SPK Barang Nama</th>
                        <th>SPK QTY</th>
                        <!-- <th style="width:150px;text-align:center;">Aksi</th> -->
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $no=0;
                        foreach($data->result_array() as $d){ 
                            $no++;
                            $id=$d['d_spk_id'];
                            $fk=$d['d_spk_nofak'];
                            $spk_brg_id=$d['d_spk_barang_id'];
                            $spk_brg_nm=$d['d_spk_barang_nama'];
                            $spk_qty=$d['d_spk_qty'];
                    ?>
                    <tr>
                        <td style="text-align:center;"><?php echo $no;?></td>
                        <td style="text-align:center;"><?php echo $id;?></td>
                        <td style="text-align:center;"><?php echo $fk;?></td>
                        <td style="text-align:center;"><?php echo $spk_brg_id;?></td>
                        <td style="text-align:center;"><?php echo $spk_brg_nm;?></td>
                        <td style="text-align:center;"><?php echo $spk_qty;?></td>
                        <!-- <td style="text-align:center;"> -->
                            <!-- <a class="btn btn-xs btn-success" href="#modalEditPelanggan<?php //echo $id?>" data-toggle="modal" title="Edit"><span class="fa fa-print"></span> Print</a> -->
                        <!-- </td> -->
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            </div>
        </div>
        <!-- /.row -->
        <hr>
        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p style="text-align:center;">Copyright &copy; <?php echo '2021';?> by E-SPK PT TELAGA HERANG</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="<?php echo base_url().'assets/js/jquery.js'?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url().'assets/dist/js/bootstrap-select.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/dataTables.bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/jquery.dataTables.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/jquery.price_format.min.js'?>"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#mydata').DataTable();
        } );
    </script>
</body>

</html>