<html lang="en" moznomarginboxes mozdisallowselectionprint>
<head>
    <title type="hidden">E-SPK || PT TELAGA HERANG</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/laporan.css')?>"/>
</head>
<body onload="window.print()">
    <?php 
    $b=$data->row_array();
    ?>
        <p style="margin-left: 20px;">DESKRIPSI KEGIATAN (LINGKUP PEKERJAAN)</p>
        <table border="0" align="center" style="width:700px;margin-bottom:20px;">
        <tbody>
            <tr>
                <th>No</th>
                <th>Id Barang</th>
                <th>Nama Barang</th>
                <th>Ukuran Barang</th>
                <th>Jumlah</th>
                <th>Tanggal</th>
            </tr>
        <?php 
        $no=0;
            foreach ($data->result_array() as $i) {
               $no++;
               
               $id=$i['d_spk_barang_id'];
               $nabar=$i['d_spk_barang_nama'];
               $ukuran=$i['ukuran'];
               $qty=$i['d_spk_qty'];
               $tgl=$i['d_spk_tanggal'];
        ?>
            <tr>
                <td style="text-align:center;"><?php echo $no;?></td>
                <td style="text-align:center;"><?php echo $id;?></td>
                <td style="text-align:center;"><?php echo $nabar;?></td>
                <td style="text-align:center;"><?php echo $ukuran;?></td>
                <td style="text-align:center;"><?php echo $qty;?></td>
                <td style="text-align:center;"><?php echo $tgl;?></td>
            </tr>
        <?php }?>
        </tbody>
        <tfoot>
        </tfoot>
        </table>
    </div>
</body>
</html>