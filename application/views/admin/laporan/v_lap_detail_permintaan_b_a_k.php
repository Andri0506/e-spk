<html lang="en" moznomarginboxes mozdisallowselectionprint>
<head>
    <title>Laporan Permintaan Barang</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/laporan.css')?>"/>
</head>
<body onload="window.print()">
<div id="laporan">
<table align="center" style="width:900px; border-bottom:3px double;border-top:none;border-right:none;border-left:none;margin-top:5px;margin-bottom:20px;">
<!--<tr>
    <td><img src="<?php// echo base_url().'assets/img/kop_surat.png'?>"/></td>
</tr>-->
</table>

<table border="0" align="center" style="width:800px; border:none;margin-top:5px;margin-bottom:0px;">
<tr>
    <td colspan="2" style="width:800px;paddin-left:20px;"><center><h4>LAPORAN Permintaan Barang</h4></center><br/></td>
</tr>
                       
</table>
 
<!-- <table border="0" align="center" style="width:900px;border:none;">
        <tr>
            <th style="text-align:left"></th>
        </tr>
</table> -->

<table border="1" align="center" style="width:800px;margin-bottom:20px;">
                <thead>
                    <tr>
                        <th style="text-align:center;width:40px;">No</th>
                        <th>Permintaan ID</th>
                        <th>Permintaan Faktur</th>
                        <th>Permintaan Barang Id</th>
                        <th>Permintaan Barang Nama</th>
                        <th>Permintaan QTY</th>
                        <th>Keterangan Permintaan</th>
                        <!-- <th style="width:150px;text-align:center;">Aksi</th> -->
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $no=0;
                        foreach($data->result_array() as $d){ 
                            $no++;
                            $id=$d['d_permintaan_id'];
                            $fk=$d['d_permintaan_barang_nofak'];
                            $permintaan_brg_id=$d['d_permintaan_barang_id'];
                            $permintaan_brg_nm=$d['d_permintaan_barang_nama'];
                            $permintaan_qty=$d['d_permintaan_qty'];
                            $keterangan_permintaan=$d['permintaan_barang_keterangan'];
                            // $permintaan_s_b=$d['status_permintaan_barang'];

                    ?>
                    <tr>
                        <td style="text-align:center;"><?php echo $no;?></td>
                        <td style="text-align:center;"><?php echo $id;?></td>
                        <td style="text-align:center;"><?php echo $fk;?></td>
                        <td style="text-align:center;"><?php echo $permintaan_brg_id;?></td>
                        <td style="text-align:center;"><?php echo $permintaan_brg_nm;?></td>
                        <td style="text-align:center;"><?php echo $permintaan_qty;?></td>
                        <td style="text-align:center;"><?php echo $keterangan_permintaan;?></td>
                        <!-- <td style="text-align:center;"> -->
                            <!-- <a class="btn btn-xs btn-success" href="#modalEditPelanggan<?php //echo $id?>" data-toggle="modal" title="Edit"><span class="fa fa-print"></span> Print</a> -->
                        <!-- </td> -->
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
<table align="center" style="width:800px; border:none;margin-top:5px;margin-bottom:20px;">
    <tr>
        <td></td>
</table>
<table align="center" style="width:800px; border:none;margin-top:5px;margin-bottom:20px;">
    <tr>
        <td align="right">Bogor, <?php echo date('d-M-Y')?></td>
    </tr>
    <tr>
        <td align="right"></td>
    </tr>
   
    <tr>
    <td><br/><br/><br/><br/></td>
    </tr>    
    <tr>
        <td align="right">( <?php echo $this->session->userdata('user');?> )</td>
    </tr>
    <tr>
        <td align="center"></td>
    </tr>
</table>
<table align="center" style="width:800px; border:none;margin-top:5px;margin-bottom:20px;">
    <tr>
        <th><br/><br/></th>
    </tr>
    <tr>
        <th align="left"></th>
    </tr>
</table>
</div>
</body>
</html>_