<html lang="en" moznomarginboxes mozdisallowselectionprint>
<head>
    <title type="hidden">E-SPK || PT TELAGA HERANG</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/laporan.css')?>"/>
</head>
<body onload="window.print()">
    <center><h1 style="margin-top: 10px;">PT TELAGA HERANG</h1></center>
    <center><h4 style="margin-top: 20px;">Jl. Raya Jakarta Bogor KM. 52 Bogor 16710</h4></center>
    <center><h3 style="margin-top: 40px;">Surat Perintah Kerja</h3><br/></center>

    <?php 
    $b=$data->row_array();
    ?>
    
    <table border="0" style="margin-left:20px">
        <tr>
            <th style="text-align:left;"><p style="font-size:10px;">Nomor</p></th>
            <th style="text-align:left;"><p style="font-size:10px; margin-left:20px;">:   <?php echo $b['no_surat'];?></p></th>
        </tr>
        <tr>
            <th style="text-align:left;"><p style="font-size:10px;">Tanggal </p></th>
            <th style="text-align:left;"><p style="font-size:10px; margin-left:20px;">: <?php echo date('d-M-Y')?></p></th>
        </tr>
        <tr>
            <th style="text-align:left;"><p style="font-size:10px;">Proyek </p></th>
            <th style="text-align:left;"><p style="font-size:10px; margin-left:20px;">: <?php echo $b['nama_pekerjaan'];?></p></th>
        </tr>
        <tr>
            <th style="text-align:left;"><p style="font-size:10px;">Lokasi</p></th>
            <th style="text-align:left;"><p style="font-size:10px; margin-left:20px;">: <?php echo $b['lokasi'];?></p></th>
        </tr>
    </table>
    <div id="laporan">
        <p style="margin-left: 20px; margin-top: 30px;">Pada hari ini <?php
            $tanggal = date('d-m-Y');
            $hari   = date('l', microtime($tanggal));
            $hari_indonesia = array('Monday'  => 'Senin',
                'Tuesday'  => 'Selasa',
                'Wednesday' => 'Rabu',
                'Thursday' => 'Kamis',
                'Friday' => 'Jumat',
                'Saturday' => 'Sabtu',
                'Sunday' => 'Minggu');
            echo $hari_indonesia[$hari];
            // Tanggal 2017-01-31 adalah hari Senin
            ?>
        Tanggal <?php echo date('d');?> Bulan <?php echo date('m');?> Tahun <?php echo date('Y');?>, yang bertanda tangan dibawah ini :
        </p>
        <table style="margin-left:20px; border: 0px; font-weight: normal;">
            <tr>
                <th style="text-align:left;"><p style="font-size:10px;">Nama</p></th>
                <th style="text-align:left;"><p style="font-size:10px; margin-left:20px;">:   <?php echo $this->session->userdata('nama');?></p></th>
            </tr>
            <tr>
                <th style="text-align:left;"><p style="font-size:10px;">Jabatan </p></th>
                <th style="text-align:left;"><p style="font-size:10px; margin-left:20px;">: Direktur Utama</p></th>
            </tr>
            <tr>
                <th style="text-align:left;"><p style="font-size:10px;">Alamat </p></th>
                <th style="text-align:left;"><p style="font-size:10px; margin-left:20px;">: Jln raya Cimanggu</p></th>
            </tr>
        </table>

        <p style="margin-left: 20px; margin-top: 10px;">Selanjutnya disebut <b>Pemberi Tugas</b></p>
        
        <table style="margin-left:20px; border: 0px; font-weight: normal;">
            <tr>
                <th style="text-align:left;"><p style="font-size:10px;">Nama</p></th>
                <th style="text-align:left;"><p style="font-size:10px; margin-left:20px;">: ....................</p></th>
            </tr>
            <tr>
                <th style="text-align:left;"><p style="font-size:10px;">Jabatan </p></th>
                <th style="text-align:left;"><p style="font-size:10px; margin-left:20px;">: ....................</p></th>
            </tr>
            <tr>
                <th style="text-align:left;"><p style="font-size:10px;">Alamat </p></th>
                <th style="text-align:left;"><p style="font-size:10px; margin-left:20px;">: ....................</p></th>
            </tr>
        </table>

        <p style="margin-left: 20px; margin-top: 10px;">Selanjutnya disebut <b>Penerima Tugas</b></p>
        <table style="margin-left:30px; border: 0px; font-weight: normal;">
            <tr>
                <th style="text-align:center;"><p style="font-size:10px;">I.</p></th>
                <th style="text-align:left;"><p style="font-size:10px; margin-left:20px;">....................</p></th>
            </tr>
            <tr>
                <th style="text-align:center;"><p style="font-size:10px;">II. </p></th>
                <th style="text-align:left;"><p style="font-size:10px; margin-left:20px;">....................</p></th>
            </tr>
            <tr>
                <th style="text-align:center;"><p style="font-size:10px;">III. </p></th>
                <th style="text-align:left;"><p style="font-size:10px; margin-left:20px;">....................</p></th>
            </tr>
            <tr>
                <th style="text-align:center;"><p style="font-size:10px;">IV. </p></th>
                <th style="text-align:left;"><p style="font-size:10px; margin-left:20px;">....................</p></th>
            </tr>
            <tr>
                <th style="text-align:center;"><p style="font-size:10px;">V. </p></th>
                <th style="text-align:left;"><p style="font-size:10px; margin-left:20px;">....................</p></th>
            </tr>
        </table>
        <p style="margin-left: 20px; margin-top: 10px;">Dengan ini Pemberi Tugas menunjuk Penerima Tugas untuk melaksanakan pekerjaan sebagaimana yang ditentukan sebagai berikut:</p>
        <p style="margin-left:20px;">Dikeluarkan di Pada Tanggal <?php echo date('d-m-Y');?></p>
        <table style="margin-left:20px; border: 0px; width: 600px;">
            <tr>
                <td style="text-align: left;">Penerima Tugas</td>
                <td style="text-align: right;">Pemberi Tugas</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td ></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td ></td>
                <td></td>
            </tr>
            <tr>
                <td ></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td ></td>
                <td></td>
            </tr>
            <tr>
                <td rowspan="4" style="text-align: left;">(...................)</td>
                <td rowspan="4" style="text-align: right;">(...................)</td>
            </tr>
        </table>
    </div>
</body>
</html>