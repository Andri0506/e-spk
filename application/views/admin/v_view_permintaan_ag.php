<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Produk By Mfikri.com">
    <meta name="author" content="M Fikri Setiadi">

    <title>E-SPK || PT TELAGA HERANG</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url().'assets/css/bootstrap.min.css'?>" rel="stylesheet">
	<!-- <link href="<?php //echo base_url().'assets/css/style.css'?>" rel="stylesheet"> -->
	<link href="<?php echo base_url().'assets/css/font-awesome.css'?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url().'assets/css/4-col-portfolio.css'?>" rel="stylesheet">
    <link href="<?php echo base_url().'assets/css/dataTables.bootstrap.min.css'?>" rel="stylesheet">
    <link href="<?php echo base_url().'assets/css/jquery.dataTables.min.css'?>" rel="stylesheet">
    <link href="<?php echo base_url().'assets/dist/css/bootstrap-select.css'?>" rel="stylesheet">
</head>

<body>
    <!-- Navigation -->
   <?php 
        $this->load->view('admin/menu');
   ?>

    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Data
                    <small>Permintaan Barang</small>
                    <div class="pull-right"><a href="<?php echo base_url().'admin/permintaan_barang/tampil_permintaan_barang'?>" class="btn btn-sm btn-primary"><span class="fa fa-backward"></span> Kembali</a></div>
                </h1>
            </div>
        </div>
        
        <!-- /.row -->
        <!-- Projects Row -->
        <div class="row">
            <div class="col-lg-12">
            <table class="table table-bordered table-condensed" style="font-size:11px;" id="mydata">
                <thead>
                    <tr>
                        <th style="text-align:center;width:40px;">No</th>
                        <th>Permintaan Id</th>
                        <th>Permintaan Faktur</th>
                        <th>Barang Id</th>
                        <th>Barang Nama</th>
                        <th width="100px">Permintaan Qty</th>
                        <!-- <th>Keterangan</th> -->
                        <!-- <th style="width:150px;text-align:center;">Aksi</th> -->
                    </tr>
                </thead>
                <tbody>
                <?php 
                    $no=0;
                    $no++;
                    foreach ($data->result_array() as $a) { 
                        // print_r($a);
                         
                         
                         $no++;
                        $id=$a['d_permintaan_id'];
                        $fk=$a['d_permintaan_barang_nofak'];
                        $brg_id=$a['d_permintaan_barang_id'];
                        $brg_nama=$a['d_permintaan_barang_nama'];
                        $p_qty=$a['d_permintaan_qty'];
                    ?>
                    <tr>
                        <td style="text-align:center;"><?php echo $no;?></td>
                        <td><?php echo $id;?></td>
                        <td><?php echo $fk;?></td>
                        <td style="text-align:center;"><?php echo $brg_id;?></td>
                        <td style="text-align:center;"><?php echo $brg_nama;?></td>
                        <td style="text-align:center;"><input type="number" name="qty" value="<?php echo $p_qty;?>" class="form-control"></td>
                        <!-- <td style="text-align:center;"> -->
                            <!-- <a class="btn btn-xs btn-warning" href="#modalEditPermintaan<?php //echo $id?>" data-toggle="modal" title="Edit"><span class="fa fa-edit"></span> Detail</a> -->
                        <!-- </td> -->
                    </tr>
                <?php };?>
                </tbody>
            </table>
            <form action="<?php echo base_url().'admin/permintaan_barang/update_permintaan_barang/'.$fk?>" method="post">
                <input type="hidden" name="d_permintaan_id" value="<?php echo $id?>">
                <input type="hidden" name="d_permintaan_barang_nofak" value="<?php echo $fk;?>">
                <input type="hidden" name="d_permintaan_qty" value="<?php echo $p_qty;?>">
                <!-- <input type="hidden" name="status_permintaan_barang" value="Approve"> -->
                <button type="submit" class="btn btn-xs btn-info" title="Approve" style="height:50px;width:100px;"><span class="fa fa-paper-plane"></span> Approve</button>
            </form>
            </div>
        </div>
        <!-- <form action="<?php //echo base_url().'admin/permintaan_barang/update_permintaan_barang'?>" method="post">
            <table>
                <tr>
                    <td><input text="hidden" name="d_permintaan_id" value="<?php //echo $id;?>"></td>
                    <td><input text="hidden" name="d_permintaan_qty" value="<?php //echo $p_qty;?>"></td>
                    <td style="width:760px;" rowspan="2"><button type="submit" class="btn btn-info btn-lg"> Approve</button></td>
                </tr>
            </table>
        </form> -->
        <!-- /.row -->
        <!-- ============ MODAL EDIT =============== -->
        <?php
                    // foreach ($data->result_array() as $a) {
                    //     $id=$a['d_permintaan_id'];
                    //     $fk=$a['d_permintaan_barang_nofak'];
                    //     $permintaan_brng_id=$a['d_permintaan_barang_id'];
                    //     $permintaan_brng_nama=$a['d_permintaan_barang_nama'];
                    //     $permintaan_qty=$a['d_permintaan_qty'];
                    //     $status_permintaan_barang=$a['status_permintaan_barang'];
                     ?>
                <div id="modalEditPermintaan<?php //echo $id?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 class="modal-title" id="myModalLabel">Edit Permintaan Barang </h3>
                    </div>
                    <form class="form-horizontal" method="post" action="<?php //echo base_url().'admin/Permintaan_barang/edit_detail_permintaan_barang'?>">
                        <div class="modal-body">
                        
                        <div class="form-group">
                            <label class="control-label col-xs-3" >Kode Permintaan Barang</label>
                            <div class="col-xs-9">
                                <input name="d_permintaan_id" class="form-control" type="text" value="<?php //echo $id;?>" placeholder="Kode Permintaan Barang..." style="width:335px;" readonly>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-xs-3" >Faktur Permintaan Barang</label>
                            <div class="col-xs-9">
                                <input name="d_permintaan_barang_nofak" class="form-control" type="text" value="<?php //echo $fk;?>" placeholder="Faktur..." style="width:335px;" readonly>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-xs-3" >Permintaan Barang Id</label>
                            <div class="col-xs-9">
                                <input name="d_permintaan_barang_id" class="form-control" type="text" value="<?php //echo $permintaan_brng_id;?>" placeholder="Permintaan Barang Id..." style="width:335px;" readonly>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-xs-3" >Permintaan barang Nama</label>
                            <div class="col-xs-9">
                                <input name="d_permintaan_barang_nama" class="form-control" type="text" value="<?php //echo $permintaan_brng_nama;?>" placeholder="Permintaan Barang Nama..." style="width:335px;" readonly>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-xs-3" >Qty</label>
                            <div class="col-xs-9">
                                <input name="d_permintaan_qty" class="form-control" type="number" value="<?php //echo $permintaan_qty;?>" placeholder="Permintaan Barang Qty..." style="width:335px;" required>
                            </div>
                        </div>

                        <!-- <div class="form-group">
                        <label class="control-label col-xs-3" >Status Permintaan Barang</label>
                        <div class="col-xs-9">
                            <select name="status_permintaan_barang" class="form-control" style="width:280px;" required>
                                <option value="Tidak Approve">Tidak Approve</option>
                                <option value="Approve">Approve</option>
                            </select>
                        </div> -->
                    </div>  

                    </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                            <button type="submit" class="btn btn-info">Update</button>
                        </div>
                    </form>
                </div>
                </div>
                </div>
            <?php
        //}
        ?>

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p style="text-align:center;">Copyright &copy; <?php echo '2021';?> by SPK || TELAGA HERANG</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="<?php echo base_url().'assets/js/jquery.js'?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url().'assets/dist/js/bootstrap-select.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/dataTables.bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/jquery.dataTables.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/jquery.price_format.min.js'?>"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#mydata').DataTable();
        } );
    </script>
    <script type="text/javascript">
        $(function(){
            $('.harpok').priceFormat({
                    prefix: '',
                    //centsSeparator: '',
                    centsLimit: 0,
                    thousandsSeparator: ','
            });
            $('.harjul').priceFormat({
                    prefix: '',
                    //centsSeparator: '',
                    centsLimit: 0,
                    thousandsSeparator: ','
            });
        });
    </script>
    
</body>

</html>
