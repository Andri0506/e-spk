-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 08, 2021 at 01:37 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_spk`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_barang`
--

CREATE TABLE `tbl_barang` (
  `barang_id` varchar(15) NOT NULL,
  `barang_nama` varchar(150) DEFAULT NULL,
  `barang_tgl_input` timestamp NULL DEFAULT current_timestamp(),
  `barang_tgl_last_update` datetime DEFAULT NULL,
  `barang_kategori_id` int(11) DEFAULT NULL,
  `barang_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_barang`
--

INSERT INTO `tbl_barang` (`barang_id`, `barang_nama`, `barang_tgl_input`, `barang_tgl_last_update`, `barang_kategori_id`, `barang_user_id`) VALUES
('BR000001', 'Flange', '2021-01-24 12:29:49', '2021-02-07 17:58:58', 58, 3),
('BR000002', 'Elbow', '2021-01-24 12:33:05', '2021-02-08 23:06:31', 55, 1),
('BR000003', 'Kawat Las Rb', '2021-01-24 12:33:25', '2021-02-08 23:01:56', 61, 3),
('BR000004', 'Pipa', '2021-02-06 13:48:36', '2021-02-08 23:01:46', 60, 3),
('BR000005', 'Dyna Bolt', '2021-02-08 15:48:58', '2021-02-08 23:06:38', 43, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_detail_permintaan_barang`
--

CREATE TABLE `tbl_detail_permintaan_barang` (
  `d_permintaan_id` int(11) NOT NULL,
  `d_permintaan_barang_nofak` varchar(15) DEFAULT NULL,
  `d_permintaan_barang_id` varchar(15) DEFAULT NULL,
  `d_permintaan_barang_nama` varchar(150) DEFAULT NULL,
  `d_permintaan_qty` int(11) DEFAULT NULL,
  `status_permintaan_barang` varchar(50) DEFAULT NULL,
  `status_pengiriman_barang` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_detail_permintaan_barang`
--

INSERT INTO `tbl_detail_permintaan_barang` (`d_permintaan_id`, `d_permintaan_barang_nofak`, `d_permintaan_barang_id`, `d_permintaan_barang_nama`, `d_permintaan_qty`, `status_permintaan_barang`, `status_pengiriman_barang`) VALUES
(16, '080221000001', 'BR000002', 'Elbow', 2, 'Approve', NULL),
(17, '080221000002', 'BR000002', 'Elbow', 2, NULL, NULL),
(18, '080221000002', 'BR000004', 'Pipa', 2, NULL, NULL),
(19, '110221000001', 'BR000002', 'Elbow', 1, NULL, NULL),
(20, '110221000002', 'BR000002', 'Elbow', 1, NULL, NULL),
(21, '110221000003', 'BR000004', 'Pipa', 1, NULL, NULL),
(22, '130221000001', 'BR000002', 'Elbow', 2, NULL, NULL),
(23, '130221000002', 'BR000001', 'Flange', 2, NULL, NULL),
(24, '130221000003', 'BR000002', 'Elbow', 1, NULL, NULL),
(25, '210221000001', 'BR000001', 'Flange', 2, NULL, NULL),
(26, '210221000001', 'BR000004', 'Pipa', 2, NULL, NULL),
(27, '210221000001', 'BR000003', 'Kawat Las Rb', 2, NULL, NULL),
(28, '210221000002', 'BR000002', 'Elbow', 1, NULL, NULL),
(29, '210221000002', 'BR000004', 'Pipa', 1, NULL, NULL),
(30, '210221000002', 'BR000005', 'Dyna Bolt', 1, NULL, NULL),
(31, '210221000003', 'BR000002', 'Elbow', 4, NULL, NULL),
(32, '210221000003', 'BR000004', 'Pipa', 3, NULL, NULL),
(33, '210221000003', 'BR000005', 'Dyna Bolt', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_detail_spk`
--

CREATE TABLE `tbl_detail_spk` (
  `d_spk_id` int(11) NOT NULL,
  `d_spk_nofak` varchar(15) DEFAULT NULL,
  `d_spk_barang_id` varchar(15) DEFAULT NULL,
  `d_spk_barang_nama` varchar(150) DEFAULT NULL,
  `ukuran` varchar(50) NOT NULL,
  `d_spk_qty` int(11) DEFAULT NULL,
  `d_spk_tanggal` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_detail_spk`
--

INSERT INTO `tbl_detail_spk` (`d_spk_id`, `d_spk_nofak`, `d_spk_barang_id`, `d_spk_barang_nama`, `ukuran`, `d_spk_qty`, `d_spk_tanggal`) VALUES
(93, '070321000001', 'BR000004', 'Pipa', '4 1/2', 3, '2021-03-07 16:14:38'),
(94, '070321000001', 'BR000003', 'Kawat Las Rb', '5 1/2', 3, '2021-03-07 16:14:38');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kategori`
--

CREATE TABLE `tbl_kategori` (
  `kategori_id` int(11) NOT NULL,
  `kategori_nama` varchar(35) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_kategori`
--

INSERT INTO `tbl_kategori` (`kategori_id`, `kategori_nama`) VALUES
(43, '1/2'),
(44, '3/4'),
(45, '1'),
(46, '1 1/4'),
(47, '1 1/2'),
(48, '2'),
(49, '2 1/4 '),
(50, '2 1/2'),
(51, '3'),
(52, '3 1/4'),
(54, '4'),
(55, '4 1/4'),
(56, '4 1/2'),
(57, '5'),
(58, '5 1/4'),
(59, '5 1/2'),
(60, '6 Inchi'),
(61, '1 box'),
(62, '2 box');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_permintaan_barang`
--

CREATE TABLE `tbl_permintaan_barang` (
  `permintaan_barang_nofak` varchar(15) NOT NULL,
  `permintaan_barang_tanggal` timestamp NULL DEFAULT current_timestamp(),
  `permintaan_barang_user_id` int(11) DEFAULT NULL,
  `permintaan_barang_keterangan` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_permintaan_barang`
--

INSERT INTO `tbl_permintaan_barang` (`permintaan_barang_nofak`, `permintaan_barang_tanggal`, `permintaan_barang_user_id`, `permintaan_barang_keterangan`) VALUES
('080221000001', '2021-02-08 16:10:04', 3, 'Disiapkan'),
('080221000002', '2021-02-08 16:11:42', 3, 'Approve'),
('110221000001', '2021-02-11 14:18:19', 3, 'Approve'),
('110221000002', '2021-02-11 14:33:15', 3, 'Approve'),
('110221000003', '2021-02-11 14:36:27', 3, 'Disiapkan'),
('130221000001', '2021-02-13 10:12:45', 3, 'Disiapkan'),
('130221000002', '2021-02-13 10:32:35', 3, 'Disiapkan'),
('130221000003', '2021-02-13 14:28:38', 3, 'Disiapkan'),
('210221000001', '2021-02-21 14:48:24', 3, 'Pengiriman'),
('210221000002', '2021-02-21 15:14:43', 3, 'Pengiriman'),
('210221000003', '2021-02-21 15:14:43', 3, 'proses');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_spk`
--

CREATE TABLE `tbl_spk` (
  `spk_nofak` varchar(15) NOT NULL,
  `no_surat` varchar(50) DEFAULT NULL,
  `nama_pekerjaan` varchar(150) DEFAULT NULL,
  `lokasi` text DEFAULT NULL,
  `spk_tanggal` timestamp NULL DEFAULT current_timestamp(),
  `spk_user_id` int(11) DEFAULT NULL,
  `spk_keterangan` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_spk`
--

INSERT INTO `tbl_spk` (`spk_nofak`, `no_surat`, `nama_pekerjaan`, `lokasi`, `spk_tanggal`, `spk_user_id`, `spk_keterangan`) VALUES
('070321000001', '0001/07/03/2021', 'ME-PP-GAS-Piping gas installation di PT. Aqua Golden Mississippi', 'Jl. Cidahu Kp. Pojok, RT.03/01 Desa Babakanpari, Kecamatan Cidahu, Kabupaten Sukabumi 43358', '2021-03-07 16:12:30', 2, 'Proses'),
('070321000002', '0001/07/03/2021', 'ME-PP-GAS-Piping gas installation di PT. Aqua Golden Mississippi', 'Jl. Cidahu Kp. Pojok, RT.03/01 Desa Babakanpari, Kecamatan Cidahu, Kabupaten Sukabumi 43358', '2021-03-07 16:13:17', 2, 'Proses');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL,
  `user_nama` varchar(35) DEFAULT NULL,
  `user_username` varchar(30) DEFAULT NULL,
  `user_password` varchar(35) DEFAULT NULL,
  `user_level` varchar(2) DEFAULT NULL,
  `user_status` varchar(15) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `user_nama`, `user_username`, `user_password`, `user_level`, `user_status`) VALUES
(1, 'Akbar', 'Akbar', '4f033a0a2bf2fe0b68800a3079545cd1', '1', '1'),
(2, 'Wandi', 'Wandi', '21232f297a57a5a743894a0e4a801fc3', '1', '1'),
(3, 'Komar', 'Komarudin', '21232f297a57a5a743894a0e4a801fc3', '2', '1'),
(4, 'Agus', 'Agus Supriadi', '21232f297a57a5a743894a0e4a801fc3', '3', '1'),
(5, 'Ahmad', 'Ahmad Saepudin', '21232f297a57a5a743894a0e4a801fc3', '4', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_barang`
--
ALTER TABLE `tbl_barang`
  ADD PRIMARY KEY (`barang_id`),
  ADD KEY `barang_user_id` (`barang_user_id`),
  ADD KEY `barang_kategori_id` (`barang_kategori_id`);

--
-- Indexes for table `tbl_detail_permintaan_barang`
--
ALTER TABLE `tbl_detail_permintaan_barang`
  ADD PRIMARY KEY (`d_permintaan_id`);

--
-- Indexes for table `tbl_detail_spk`
--
ALTER TABLE `tbl_detail_spk`
  ADD PRIMARY KEY (`d_spk_id`),
  ADD KEY `d_spk_barang_id` (`d_spk_barang_id`),
  ADD KEY `d_spk_nofak` (`d_spk_nofak`);

--
-- Indexes for table `tbl_kategori`
--
ALTER TABLE `tbl_kategori`
  ADD PRIMARY KEY (`kategori_id`);

--
-- Indexes for table `tbl_permintaan_barang`
--
ALTER TABLE `tbl_permintaan_barang`
  ADD PRIMARY KEY (`permintaan_barang_nofak`),
  ADD KEY `permintaan_barang_user_id` (`permintaan_barang_user_id`);

--
-- Indexes for table `tbl_spk`
--
ALTER TABLE `tbl_spk`
  ADD PRIMARY KEY (`spk_nofak`),
  ADD KEY `spk_user_id` (`spk_user_id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_detail_permintaan_barang`
--
ALTER TABLE `tbl_detail_permintaan_barang`
  MODIFY `d_permintaan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `tbl_detail_spk`
--
ALTER TABLE `tbl_detail_spk`
  MODIFY `d_spk_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;

--
-- AUTO_INCREMENT for table `tbl_kategori`
--
ALTER TABLE `tbl_kategori`
  MODIFY `kategori_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_barang`
--
ALTER TABLE `tbl_barang`
  ADD CONSTRAINT `tbl_barang_ibfk_1` FOREIGN KEY (`barang_user_id`) REFERENCES `tbl_user` (`user_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_barang_ibfk_2` FOREIGN KEY (`barang_kategori_id`) REFERENCES `tbl_kategori` (`kategori_id`) ON UPDATE CASCADE;

--
-- Constraints for table `tbl_detail_spk`
--
ALTER TABLE `tbl_detail_spk`
  ADD CONSTRAINT `tbl_detail_spk_ibfk_1` FOREIGN KEY (`d_spk_barang_id`) REFERENCES `tbl_barang` (`barang_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_detail_spk_ibfk_2` FOREIGN KEY (`d_spk_nofak`) REFERENCES `tbl_spk` (`spk_nofak`) ON UPDATE CASCADE;

--
-- Constraints for table `tbl_permintaan_barang`
--
ALTER TABLE `tbl_permintaan_barang`
  ADD CONSTRAINT `tbl_permintaan_barang_ibfk_1` FOREIGN KEY (`permintaan_barang_user_id`) REFERENCES `tbl_user` (`user_id`) ON UPDATE CASCADE;

--
-- Constraints for table `tbl_spk`
--
ALTER TABLE `tbl_spk`
  ADD CONSTRAINT `tbl_spk_ibfk_1` FOREIGN KEY (`spk_user_id`) REFERENCES `tbl_user` (`user_id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
